/*
  Warnings:

  - The `is_banned` column on the `users` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "users" DROP COLUMN "is_banned",
ADD COLUMN     "is_banned" BOOLEAN NOT NULL DEFAULT false;
