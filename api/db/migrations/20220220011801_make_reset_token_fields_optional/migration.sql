-- AlterTable
ALTER TABLE "users" ALTER COLUMN "resetToken" DROP NOT NULL,
ALTER COLUMN "resetTokenExp" DROP NOT NULL;
