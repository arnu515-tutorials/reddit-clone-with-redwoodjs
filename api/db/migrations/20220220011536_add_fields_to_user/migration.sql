/*
  Warnings:

  - Added the required column `resetToken` to the `users` table without a default value. This is not possible if the table is not empty.
  - Added the required column `resetTokenExp` to the `users` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "users" ADD COLUMN     "resetToken" VARCHAR(255) NOT NULL,
ADD COLUMN     "resetTokenExp" TIMESTAMPTZ NOT NULL;
