import { MetaTags } from '@redwoodjs/web'

const HomePage = () => {
  return (
    <>
      <MetaTags
        title="Redwoodit"
        description="A clone of Reddit using RedwoodJS"
      />

      <h1>Redwoodit</h1>
    </>
  )
}

export default HomePage
